package pl.piotrwierzbowski.rover.model;

public class Rover {

	private static final char TURN_RIGHT_COMMAND = 'R';
	private static final char TURN_LEFT_COMMAND = 'L';
	private static final char BACKWARD_COMMAND = 'B';
	private static final char FORWARD_COMMAND = 'F';
	
	private final Coordinates coordinates;

	public Rover(Coordinates coordinates) {
		this.coordinates = coordinates;
	}
	
	public void executeCommands(String commands) {
		for(var index = 0; index < commands.length(); index++) {
			executeCommand(commands.charAt(index));
		}
	}
	
	protected void executeCommand(char command) {
		switch (command) {
			case FORWARD_COMMAND :
				moveForward();
				break;
				
			case BACKWARD_COMMAND :
				moveBackward();
				break;
				
			case TURN_LEFT_COMMAND :
				turnLeft();
				break;
				
			case TURN_RIGHT_COMMAND :
				turnRight();
				break;

			default :
				System.err.println("Unknown command '" + command + "' - ignoring");
				break;
		}
	}

	protected void turnRight() {
		coordinates.setDirection(coordinates.getDirection().getRightDirection());
	}

	protected void turnLeft() {
		coordinates.setDirection(coordinates.getDirection().getLeftDirection());
	}

	protected void moveBackward() {
		coordinates.setX(coordinates.getX() - coordinates.getDirection().getIncrementOfX());
		coordinates.setY(coordinates.getY() - coordinates.getDirection().getIncrementOfY());
	}

	protected void moveForward() {
		coordinates.setX(coordinates.getX() + coordinates.getDirection().getIncrementOfX());
		coordinates.setY(coordinates.getY() + coordinates.getDirection().getIncrementOfY());
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}
}
