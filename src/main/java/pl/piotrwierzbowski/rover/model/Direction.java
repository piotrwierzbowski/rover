package pl.piotrwierzbowski.rover.model;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Stream;

public enum Direction {
	NORTH(0, 1, 0), 
	EAST(1, 0, 1), 
	SOUTH(0, -1, 2), 
	WEST(-1, 0, 3);
	
	private int incrementOfX;
	private int incrementOfY;
	private int ordinal;
	
	Direction(int x, int y, int ordinal) {
		this.incrementOfX = x;
		this.incrementOfY = y;
		this.ordinal = ordinal;
	}

	public int getIncrementOfX() {
		return incrementOfX;
	}

	public int getIncrementOfY() {
		return incrementOfY;
	}

	public Direction getLeftDirection() {
		int newOrdinal = (4 + this.ordinal - 1) % 4;
		Direction newDirection = getDirectionByOrdinal(newOrdinal)
			.orElseThrow(() -> new NoSuchElementException("No such element with ordinal " + newOrdinal));
		return newDirection;
	}
	
	public Direction getRightDirection() {
		int newOrdinal = (this.ordinal + 1) % 4;
		Direction newDirection = getDirectionByOrdinal(newOrdinal)
			.orElseThrow(() -> new NoSuchElementException("No such element with ordinal " + newOrdinal));
		return newDirection;
	}
	
	protected static Optional<Direction> getDirectionByOrdinal(int ordinal) {
		return Stream.of(Direction.values())
				.filter(direction -> direction.ordinal == ordinal)
				.findFirst();
	}
}
