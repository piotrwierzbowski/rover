package pl.piotrwierzbowski.rover;

import pl.piotrwierzbowski.rover.model.Coordinates;
import pl.piotrwierzbowski.rover.model.Direction;
import pl.piotrwierzbowski.rover.model.Rover;

public class RoverKataApplication {

	public static void main(String[] args) {
		if (args.length != 4) {
			throw new IllegalArgumentException("Expecting 4 arguments: [x y direction commands]");
		}
		
		int x = Integer.parseInt(args[0]);
		int y = Integer.parseInt(args[1]);
		Direction direction = Direction.valueOf(args[2]);
		String commands = args[3];
		
		Rover rover = new Rover(new Coordinates(x, y, direction));
		rover.executeCommands(commands);
		System.out.println("Reached: " + rover.getCoordinates());
	}

}
