package pl.piotrwierzbowski.rover.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RoverTest {

	@Test
	void testExecuteCommands() {
		// given
		Coordinates initialCoordiantes = new Coordinates(0, 0, Direction.NORTH);
		Rover rover = new Rover(initialCoordiantes);
		
		// when
		rover.executeCommands("FFF");
		
		// then
		assertEquals(0, rover.getCoordinates().getX());
		assertEquals(3, rover.getCoordinates().getY());
		assertEquals(Direction.NORTH, rover.getCoordinates().getDirection());
	}
	
	@Test
	void testExecuteCommandsNotFailing() {
		// given
		Coordinates initialCoordiantes = new Coordinates(0, 0, Direction.NORTH);
		Rover rover = new Rover(initialCoordiantes);
		
		// when
		rover.executeCommands("this is a nonsense series of commands");
		
		// then
		assertEquals(0, rover.getCoordinates().getX());
		assertEquals(0, rover.getCoordinates().getY());
		assertEquals(Direction.NORTH, rover.getCoordinates().getDirection());
	}

	@Test
	void testTurnRight() {
		// given
		Coordinates initialCoordiantes = new Coordinates(0, 0, Direction.NORTH);
		Rover rover = new Rover(initialCoordiantes);
		
		// when
		rover.executeCommands("R");
		
		// then
		assertEquals(0, rover.getCoordinates().getX());
		assertEquals(0, rover.getCoordinates().getY());
		assertEquals(Direction.EAST, rover.getCoordinates().getDirection());
	}

	@Test
	void testTurnLeft() {
		// given
		Coordinates initialCoordiantes = new Coordinates(0, 0, Direction.NORTH);
		Rover rover = new Rover(initialCoordiantes);
		
		// when
		rover.executeCommands("L");
		
		// then
		assertEquals(0, rover.getCoordinates().getX());
		assertEquals(0, rover.getCoordinates().getY());
		assertEquals(Direction.WEST, rover.getCoordinates().getDirection());
	}

	@Test
	void testMoveBackward() {
		// given
		Coordinates initialCoordiantes = new Coordinates(0, 0, Direction.NORTH);
		Rover rover = new Rover(initialCoordiantes);
		
		// when
		rover.executeCommands("B");
		
		// then
		assertEquals(0, rover.getCoordinates().getX());
		assertEquals(-1, rover.getCoordinates().getY());
		assertEquals(Direction.NORTH, rover.getCoordinates().getDirection());
	}

	@Test
	void testMoveForward() {
		// given
		Coordinates initialCoordiantes = new Coordinates(0, 0, Direction.NORTH);
		Rover rover = new Rover(initialCoordiantes);
		
		// when
		rover.executeCommands("F");
		
		// then
		assertEquals(0, rover.getCoordinates().getX());
		assertEquals(1, rover.getCoordinates().getY());
		assertEquals(Direction.NORTH, rover.getCoordinates().getDirection());
	}

}
