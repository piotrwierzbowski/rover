package pl.piotrwierzbowski.rover.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;

class DirectionTest {

	@Test
	void testGetLeftDirection() {
		// arrange and assert
		assertEquals(Direction.WEST, Direction.NORTH.getLeftDirection());
		assertEquals(Direction.SOUTH, Direction.WEST.getLeftDirection());
		assertEquals(Direction.EAST, Direction.SOUTH.getLeftDirection());
		assertEquals(Direction.NORTH, Direction.EAST.getLeftDirection());
	}

	@Test
	void testGetRightDirection() {
		// arrange and assert
		assertEquals(Direction.EAST, Direction.NORTH.getRightDirection());
		assertEquals(Direction.NORTH, Direction.WEST.getRightDirection());
		assertEquals(Direction.WEST, Direction.SOUTH.getRightDirection());
		assertEquals(Direction.SOUTH, Direction.EAST.getRightDirection());
	}

	@Test
	void testGetDirectionByOrdinal() {
		// arrange and assert
		assertEquals(Optional.of(Direction.NORTH), Direction.getDirectionByOrdinal(0));
		assertEquals(Optional.of(Direction.EAST), Direction.getDirectionByOrdinal(1));
		assertEquals(Optional.of(Direction.SOUTH), Direction.getDirectionByOrdinal(2));
		assertEquals(Optional.of(Direction.WEST), Direction.getDirectionByOrdinal(3));
		assertEquals(Optional.empty(), Direction.getDirectionByOrdinal(4));
	}

}
