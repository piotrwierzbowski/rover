## Assignment task
Based on the description [in the file](Candidate's_Instructions_-_Mars_Rover_Kata.pdf)

## Running the program: 
### Requirements:
Java 11 JDK installed

Gradle

### Execution:
Download the repository from git

Execute:
`./gradlew clean build run --args="x y direction commands"`

where:
- x is initial x coordinate
- y is initial y coordiante
- direction is initial direction of the rover: NORTH, EAST, SOUTH or WEST
- commands is a sequence of commands to execute: FBLR format

for example:
`./gradlew clean build run --args="4 2 EAST FLFFFRFLB"`

after the first execution you can simply run it without building as: 
`./gradlew run --args="x y direction commands"`

### Testing:
Execute:
`./gradlew clean build test`